import React, { useState, useEffect } from 'react';
import { Card, Grid, Header, Icon, Label, Loader } from 'semantic-ui-react';
import axios from 'axios';
import './App.css';

/**
 * Funnção App -
 */
function App() {

  // Definindo os estados
  let [filteredData, setFilteredData] = useState(null);
  let [isLoading, setIsLoading] = useState(true);
  let [hasError, setHasError] = useState({ error: false, message: ''});

  // Pegando dados da API e atualizando o status de loading
  useEffect(() => {
    const fetchData = async() => {
      try {
        const result = await axios('http://localhost:4000/api/v1/games');
        setFilteredData(result.data.data);
      } catch (error) {
        setHasError({ error: true, message: `${error}` });
      }
      setIsLoading(false);
    };
    fetchData();
  }, [])

  if (hasError.error) {
    return (
        <Grid container verticalAlign='middle' columns={6} centered className='grid-container' data-testid='hasError'>
          <Grid.Column className='grid-container-col' textAlign='center'>
            <Header size='small'>{hasError.message}</Header>
            <Header size='small'>Try again later</Header>
          </Grid.Column>
        </Grid>  
    );
  }

  if (isLoading) {
    return (
      <Grid container verticalAlign='middle' columns={2} centered className='grid-container' data-testid='isLoading'>
        <Grid.Column className='grid-container-col'>
          <Loader active inline='centered' />
          <Header size='small'></Header>
        </Grid.Column>
      </Grid>
    );
  }

  return (
    <Grid container columns={12} className='grid-container' data-testid="isLoaded">
      {Object.keys(filteredData).map((key, idx) =>
        <Grid.Column className='grid-container-col' computer={5} tablet={6} mobile={12}>
          <Card centered fluid key={idx} >
            <Card.Content header={`Game ${idx + 1}`} />
            <Card.Content className="card-container">
              <Header size='small'>Players</Header>
              {filteredData[key].players && filteredData[key].players.map((player, id) => 
                <Label.Group key={id} color='teal'>
                  <Label>
                    {player}
                    <Label.Detail> kills: {filteredData[key].kills[player]}</Label.Detail>
                  </Label>  
                </Label.Group>
              )}
            </Card.Content>
            <Card.Content extra>
              <Icon name='arrow circle down' />
              Total Kills: {filteredData[key].total_kills}
            </Card.Content>
          </Card>
        </Grid.Column>
      )}
    </Grid>
  );
}

export default App;
