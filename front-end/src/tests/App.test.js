import React from 'react';
import ReactDOM from 'react-dom';
import App from '../views/App';
import { render, fireEvent, getByTestId, getByLabelText, queryByTestId} from '@testing-library/react';


it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it("App loads with initial state isLoading true", () => {
  const { container } = render(<App />);
  const isLoading = getByTestId(container, "isLoading");
  expect(isLoading).toBeTruthy();
});

it("App loads with initial state hasError false", () => {
  const { container } = render(<App />);
  const hasError = queryByTestId(container, "hasError");
  expect(hasError).toBeNull();
});

it("App loads with initial state loaded false", () => {
  const { container } = render(<App />);
  const isLoaded = queryByTestId(container, "isLoaded");
  expect(isLoaded).toBeNull();
});