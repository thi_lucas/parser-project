# Parser Project

Quake parser project

### Pré Requisitos

```
NodeJS (Versão 10.0.0 ou acima)
```

### Instalação

Após realizar a clonagem do projeto ou descompressão do zip, entre na pasta do projeto  e execute o código abaixo para instalar as dependências iniciais.

```
npm install
```

Após a instalação inicial execute o código abaixo para o setup das dependências do front-end e back-end do projeto

```
npm run setup
```

#### Iniciando Em Dev mode
Após a finalização do código acima, inicie a aplicação em modo de desenvolvimento executando o código abaixo. 
>A tela do front-end react abrirá automaticamente
>O **Back-end** utiliza a porta **4000**
>O **Front-end** utiliza a porta **3000**

```
npm run dev
```

#### Iniciando Em Prod mode
Após a finalização do código acima, inicie a aplicação em modo de produção executando o código abaixo.
>O **Back-end** utiliza a porta **4000**
>O **Back-end** utilizará o **Front-end** estaticamente portanto basta acessar o http://localhost:4000

```
npm start
```

## Execução de testes

A aplicação usa as bibliotecas Mocha e Chai para realização dos testes unitários com Istanbul para validação da taxa de cobertura no back-end e utiliza as bibliotecas Jest e testing-library/react para os testes unitários do front-end.

### Testes do back-end

Testa as funções de Models e Controllers assim como as rotas.

```
npm run test-back
```

### Testes do front-end

Testa o componente inicial e suas alterações de estado.

```
npm run test-front
```
> **Obs:** Existe um erro conhecido na biblioteca testing-library/react no qual após a conclusão do teste é mostrado um warning. 
> [Mais informações](https://github.com/testing-library/react-testing-library/issues/281) .

## Estrutura do projeto
```
├── back-end
│   ├── games.log
│   ├── nodemon.json
│   ├── package-lock.json
│   ├── package.json
│   └── src
│       ├── app.js
│       ├── bin
│       │   └── www.js
│       ├── controllers
│       │   └── GameController
│       │       ├── GameController.js
│       │       └── index.js
│       ├── models
│       │   ├── FileParser
│       │   │   ├── FileParser.js
│       │   │   └── index.js
│       │   ├── Game
│       │   │   ├── Game.js
│       │   │   └── index.js
│       │   └── Player
│       │       ├── Player.js
│       │       └── index.js
│       ├── routes
│       │   └── api.js
│       └── tests
│           ├── controllers
│           │   └── gameController.test.js
│           ├── models
│           │   ├── FileParser.test.js
│           │   ├── Game.test.js
│           │   └── Player.test.js
│           └── routes
│               └── api.test.js
├── front-end
│   ├── package-lock.json
│   ├── package.json
│   ├── public
│   │   ├── favicon.ico
│   │   ├── index.html
│   │   └── manifest.json
│   ├── src
│   │   ├── index.css
│   │   ├── index.js
│   │   ├── serviceWorker.js
│   │   ├── tests
│   │   │   └── App.test.js
│   │   └── views
│   │       ├── App.css
│   │       └── App.js
│   └── yarn.lock
├── package-lock.json
├── package.json
└── readme.md
```

### Documentação Backend
```
http://yourlocalhost:port/docs 
```

## Tecnologias utilizadas

* [ExpressJS](expressjs.com) - Framework NodeJs
* [Mocha](https://mochajs.org/) - Test Framework
* [Chai](https://www.chaijs.com/) - Assertion Library
* [React](https://reactjs.org/) - A JavaScript library for building user interfaces

## Autor

* Thiago Evangelista
