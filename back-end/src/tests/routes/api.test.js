import chai from 'chai';
import chaiHttp from 'chai-http';
import app from 'app';

chai.use(chaiHttp);
chai.should();

describe('Games HTTP', () => {
  describe('GET all games /', () => {
    it("should get all games", done => {
      chai.request(app)
        .get('/api/v1/games')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        })
    })
  })
});

describe('Wrong paths', () => {
  it('should respond with 404', done => {
    chai.request(app)
      .get('/api/v1/random')
      .end((err, res) => {
        res.should.have.status(404);
        done();
      })
  });
});
