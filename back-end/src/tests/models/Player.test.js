import chai from 'chai';
import { Player } from 'models/Player';

const should = chai.should();

describe('Testes Model Player', () => {

  let player;
  beforeEach(() => {
    player = new Player(1);
  });

  describe('Manipular número de assasinatos (kills) do Player', () => {
    it('deve checar se foi criado player', done => {
      let playerTest = new Player(2);
      playerTest.should.eql({
        _id: 2,
        _username: '',
        kills: 0
      });
      done();
    });
    it('deve checar se foi adicionado ao número de assassinatos total do Player', done => {
      player.kills.should.equal(0);
      player.increaseKills();
      player.kills.should.equal(1);
      done();
    });
    it('deve checar se foi removido do número de assassinatos total do Player', done => {
      player.kills.should.equal(0);
      player.decreaseKills();
      player.kills.should.equal(0);
      player.increaseKills();
      player.increaseKills();
      player.decreaseKills();
      player.kills.should.equal(1);
      done();
    });
    it('deve checar se foi alterado (set) o nome do Player', done => {
      player._username = '';
      player._username.should.equal('');
      player._username = 'Player 1';
      player._username.should.equal('Player 1');
      player.should.eql({
        _id: 1,
        _username: 'Player 1',
        kills: 0
      });
      done();
    });
  });
});