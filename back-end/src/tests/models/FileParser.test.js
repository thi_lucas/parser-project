import chai from 'chai';
import { FileParser } from 'models/FileParser';

const should = chai.should();
const fileParser = new FileParser();

const lines = {
  initGame: '  0:00 InitGame: ',
  clientConnect: ' 20:38 ClientConnect: 2  ',
  clientUserInfoChanged: ' 20:34 ClientUserinfoChanged: 2 n\Isgalamido\t\0\model  ',
  kill: ' 22:06 Kill: 2 3 7: Isgalamido killed Mocinha by MOD_ROCKET_SPLASH  ',
  worldKill: ' 21:42 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT  '
}

describe('Testes Model FileParser', () => {

  describe('Leitura do arquivo de log (async)', () => {
    it('deve checar se o arquivo existe', done => {
      const readFile = fileParser.readFile();
      readFile.catch(err => {
        done();
      });
    })
  });

  describe('Análise dos comandos do log', () => {
    it('deve checar se ação do comando foi correta', done => {
      let command = fileParser.parseCommand(lines.initGame);
      fileParser.commandAction(command[1], lines.initGame);
      fileParser.games.should.to.have.length(1);
      
      command = fileParser.parseCommand(lines.clientConnect);
      fileParser.commandAction(command[1], lines.clientConnect);
      fileParser.currentGame.players.should.to.have.length(1);

      command = fileParser.parseCommand(lines.kill);
      fileParser.commandAction(command[1], lines.kill);
      fileParser.currentGame.total_kills.should.equal(1);

      done();
    })
  });

  describe('Análise dos regexes do parser', () => {
    
    it('deve checar se comando foi identificado corretamente', done => {
      let command = fileParser.parseCommand(lines.initGame);
      command[1].should.equal('InitGame');
      command = fileParser.parseCommand(lines.clientConnect);
      command[1].should.equal('ClientConnect');
      command = fileParser.parseCommand(lines.clientUserInfoChanged);
      command[1].should.equal('ClientUserinfoChanged');
      command = fileParser.parseCommand(lines.kill);
      command[1].should.equal('Kill');
      done();
    })

    it('deve checar parser Id do player retorna Id correto', done => {
      fileParser.parsePlayerId(lines.clientConnect).should.equal('2');
      done();
    });

    it('deve checar parser adiciona kills corretamente', done => {
      fileParser.parsePlayerId(lines.kill);
      fileParser.currentGame.total_kills.should.equal(1);
      done();
    });
  });

});
