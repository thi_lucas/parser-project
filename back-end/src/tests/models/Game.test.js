import chai from 'chai';
import { Game } from 'models/Game';

const should = chai.should();

describe('Testes Model Game', () => {

  let game;
  beforeEach(() => {
    game = new Game();
  });

  describe('Adicionar Players ao Game', () => {
    it('deve checar se player foi adicionado ao game com sucesso', done => {
      game.addPlayer(1);
      game.players.should.to.satisfy(players => !!players.find(player => player._id === 1));
      game.addPlayer(2);
      game.players.should.to.satisfy(players => !!players.find(player => player._id === 1));
      done();
    });
    it('deve checar se player não foi adicionado ao game duplicadamente', done => {
      game.addPlayer(1);
      game.addPlayer(1);
      game.players.should.to.satisfy(players => {
        let values = players.map(player => player._id);
        return values.length === new Set(values).size;
      });
      done();
    })
    it('deve checar se id foi providenciado para a criação', done => {
      game.addPlayer();
      game.players.should.to.satisfy(players => !players.find(player => player._id === undefined));
      done();
    });
  });

  describe('Retornar Player pelo id', () => {
    it('deve checar se id foi providenciado', done => {
      should.equal(game.getPlayerById(), undefined);
      done();
    });
    it('deve checar se retornou o Player com sucesso', done => {
      game.addPlayer(50);
      game.getPlayerById(50).should.equal(game.players.find(player => player._id === 50));
      game.addPlayer(51);
      game.getPlayerById(51).should.equal(game.players.find(player => player._id === 51));
      done();
    });
  });

  describe('Retornar nome dos Players do Game', () => {
    it('deve checar se retornou os nomes dos Players do Game', done => {
      let player;

      game.getGamePlayers().should.to.have.length(0);
      game.addPlayer(1);
      game.getGamePlayers().should.to.have.length(1);

      player = game.getPlayerById(1);
      player._username = 'Player 1';
      game.getGamePlayers().should.eql([ 'Player 1' ]);

      game.addPlayer(2);
      player = game.getPlayerById(2);
      player._username = 'Player 2';
      game.getGamePlayers().should.eql([ 'Player 1', 'Player 2' ]);
      
      game.getGamePlayers().should.to.have.length(2);

      done();
    });
  });

  describe('Retornar número de assassinatos (kills) dos Players', () => {
    it('deve checar se retornou os assassinatos dos Players', done => {

      const noKillData = { 'Player 1': 0 , 'Player 2': 0 };
      const killData = { 'Player 1': 2 , 'Player 2': 2 };
      let playerOne, playerTwo;

      game.addPlayer(1);
      game.addPlayer(2);

      playerOne = game.getPlayerById(1);
      playerOne._username = 'Player 1';

      playerTwo = game.getPlayerById(2);
      playerTwo._username = 'Player 2';

      game.getPlayersKills().should.eql(noKillData);

      playerOne.increaseKills();
      playerOne.increaseKills();

      playerTwo.increaseKills();
      playerTwo.increaseKills();

      game.getPlayersKills().should.eql(killData);

      done();
    });
  });

  describe('Adicionar número total de mortes do Game', () => {
    it('deve checar se foi adicionado ao número de mortes total do Game', done => {
      game.total_kills.should.equal(0);
      game.increaseKill();
      game.total_kills.should.equal(1);
      done();
    });
  });

});