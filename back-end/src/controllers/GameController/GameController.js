import { FileParser } from 'models/FileParser';
import path from 'path';


/**
 * Classe do GameController
 */
class GameController {

  /**
   * Lista todos os games do arquivo
   * @param {*} req 
   * @param {*} res 
   * @return {json}
   */
  getGames(req, res) {
    const fileParser = new FileParser();
    const games = fileParser.readFile(path.join(__dirname, '../../../games.log'));
    games.then(data => {
      return res.status(200).json({ success: true, message: 'Games retrieve successfully', data: data });
    }).catch(err => {
      return res.status(500).json({ success: false, message: 'Games not retrived successfully', error: err });
    });
  }
}


export default GameController;
