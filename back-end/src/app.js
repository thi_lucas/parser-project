import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import apiRouter from 'routes/api';

const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../../front-end/build')));
app.use(express.static(path.join(__dirname, '../docs')));

// CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:3000"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use('/api/v1', apiRouter);

//docs
app.get('/docs', (req, res) => {
  res.sendFile('index.html', { root: path.join(__dirname, '../docs')});
})

// 404 handler
app.use((req, res, next) => res.status(404).json({ success: false, message: `Route ${req.url} not found` }));

// 500 handler
app.use((err, req, res, next) => res.status(500).json({ success: false, message: `Server error`, error: err }));

app.listen(port, () => console.log(`Quake API is listening on port ${port}!`))

export default app;
