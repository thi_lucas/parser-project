import { Player } from 'models/Player';

/**
 * Classe que representa o Game
 */
class Game {

  /**
   * Criar novo Game
   */
  constructor() {
    this.players = [];
    this.total_kills = 0;
  }

  /**
   * Adiciona +1 morte (kill) para ao total do Game
   */
  increaseKill() {
    this.total_kills ++;
  }

  /**
   * Cria novo Player e adiciona ao atributo players do Game
   * @param {number} id 
   */
  addPlayer(id) {
    if (!this.getPlayerById(id) && id) {
      const player = new Player(id);
      this.players.push(player);
    }
  }

  /**
   * Retorna o Player pelo id
   * @param {number} id 
   * @return {Player} Player
   */
  getPlayerById(id) {
    return this.players.find( player => player._id === id);
  }

  /**
   * Retorna o nome dos playes
   * @example ['Player 1', 'Player 2']
   * @return {array} Username dos Players
   */
  getGamePlayers() {
    return this.players.map(player => player._username);
  }

  /**
   * Retorna quantidade de morte dos players
   * @example { 'Player 1': 1, 'Player 2': 3 }
   * @return {object} Quantidade de mortes dos Players
   */
  getPlayersKills() {
    return this.players.reduce((acc, current) => {
        const obj = {[`${current._username}`]: current.kills};
        return ({...acc, ...obj});
      }, 
    {});
  }

}

export default Game;
