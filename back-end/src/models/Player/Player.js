/** Classe que representa o Player */
class Player {

  /**
   * Criar novo Player
   * @param {number} id 
   */
  constructor(id) {
    this._id = id;
    this._username = '';
    this.kills = 0;
  }

  /**
   * Muda o username do player
   * @param {string} newusername - Novo Username 
   * @memberof Player
   */
  set username(newusername) {
    this._username = newusername;
  }

  /**
   * Adiciona +1 morte (kill) para o player
   */
  increaseKills() {
    this.kills++;
  }

  /**
   * Remove -1 morte (kill) para o player
   */
  decreaseKills() {
    if (this.kills > 0) {
      this.kills--;
    }
  }

}

export default Player;
