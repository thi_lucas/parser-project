import fs from 'fs';
import { once } from 'events';
import readline from 'readline';
import { Game } from 'models/Game';

/**
 * Enum dos regex utilizados
 * @readonly
 * @enum {regex}
 */
const regex = {
  getCommand: /^.{0,7}([a-z A-Z][^:]*)/,  //^.{0,7}\K([a-z A-Z][^:]*)
  getPlayerId: /Client(Connect|UserinfoChanged): ([0-9]*)/,
  getPlayerUsername: /ClientUserinfoChanged: [0-9]* n\\(.*)\\t\\[0-9]+\\model/,
  getKills: /Kill: ([0-9]+) ([0-9]+)/,
}

/**
 * Classe que repesenta o FileParser
 */
class FileParser {

  /**
   * Criar nova instância do FileParser
   */
  constructor() {
    this.games = [];
    this.currentGame = null;
    this.isLoading = true;
  }

  /**
   * Lê arquivo de log linha a linha
   * @param {string} Caminho para o arquivo 
   * @example { game_1: { total_kills: 1, players: ['Player 1', 'Player 2'], kills: { 'Player 1': 1, 'Player 2': 0 }}}
   * @return {object} Games list
   */
  async readFile(file) {
    const readInterface = readline.createInterface({
      input: fs.createReadStream(file),
      output: process.stdout,
      console: false,
      terminal: false,
    });

    readInterface.on('line', (line) => {
      let command = this.parseCommand(line);
      if (!!command) {
        this.commandAction(command[1], line);
      }
    });

    await once(readInterface, 'close');

    let obj = {};
    this.games.map((game, i) => {
      obj[`game_${i + 1}`] = {
        total_kills: game.total_kills,
        players: game.getGamePlayers(),
        kills: game.getPlayersKills(),
      }
    });
    return obj;
  }

  /**
   * Checa o comando do log e retorna a ação necessária
   * @param {string} command 
   * @param {string} line 
   */
  commandAction(command, line) {
    switch(command) {
      case 'InitGame':
        this.currentGame = new Game();
        this.games.push(this.currentGame);
        break;
      case 'ClientConnect':
        let playerId = this.parsePlayerId(line);
        this.currentGame.addPlayer(playerId);
        break;
      case 'ClientUserinfoChanged':
        this.parsePlayerUserName(line);
        break;
      case 'Kill':
        this.parseKills(line);
        break;
      default:
        break;
    }
  }

   /**
   * Interpreta o Id do Player
   * @param {string} data 
   * @return {string} id
   */
  parseCommand(data) {
    return data.match(regex.getCommand);
  }

  /**
   * Interpreta o Id do Player
   * @param {string} data 
   * @return {number} id
   */
  parsePlayerId (data) {
    let playerId = data.match(regex.getPlayerId);
    if (playerId) {
      return playerId[2];
    }
    return 0;
  }

  /**
   * Interpreta o Username do Player
   * @param {string} data 
   * @return {string} username
   */
  parsePlayerUserName(data) {
    let username = data.match(regex.getPlayerUsername);
    let playerId = this.parsePlayerId(data);
    let player = this.currentGame.getPlayerById(playerId);
    if (username && player) {
      player._username = username[1];
      return username[1];
    }
  }

  /**
   * Interpreta os assassinatos (kills) do Player
   * @param {string} data 
   */
  parseKills(data) {
    let players = data.match(regex.getKills);

    if (players) {
      let killer = players[1];
      let killed = players[2];
      if(killer != 1022) {
        let player = this.currentGame.getPlayerById(killer);
        player.increaseKills();
      } else {
        let player = this.currentGame.getPlayerById(killed);
        player.decreaseKills();
      }
    }
    this.currentGame.increaseKill();
  }
}

export default FileParser;