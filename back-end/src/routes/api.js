import express from 'express';
import { GameController } from 'controllers/GameController';

const api = express.Router();
const gameController = new GameController();

/* GET home page. */
api.get('/games', gameController.getGames.bind(gameController));

export default api;
